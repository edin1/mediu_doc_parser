import json
from helpers import *
import mediu_doc_parser

LIST = [
    # ("IHIS1033-short.doc", 5),
    # ("IUQR1224-short.doc", 5),

    # ("IAQD2013.doc", 4),
    # ("IHIS2043.doc", 3),
    # ("IFIQ2033.doc", 4),
    # ("IFIQ2113.doc", 5),
    # ("IHDT2013.doc", 5),
    # ("IFIQ2013.doc", 3),
    # ("EMET3014.doc", 5),

    # ("IHIS1033.doc", 5),
    # ("IUQR1224.doc", 5),

    # ("EMET1033.doc", 5),
    # ("IHIS1063.doc", 4),
    # ("IUSU1043.doc", 5),
    # ("LARB1014.doc", 5),
    # ("LARB1263.doc", 4),

    # ("LARB1023.doc", 5),
    # ("EMET1043.doc", 3),
    # ("IAQD1114.doc", 4),
    # ("IUHD1153.doc", 3),

    # ("LARB2213.doc", 5),
    # ("LARB2113.doc", 5),
    # ("LARB2033.doc", 4),
    # ("LARB2193.doc", 5),
    # ("LARB2283.doc", 3),
    
    # ("IUQR2014.doc", 5),
    # ("IAQD2023.doc", 1),
    # ("IUQR2024.doc", 5),
    # ("IUQR2033.doc", 5),
	# ('LARB2063.doc', 3),
	# ('IUQR2033.doc', 5),
	# ('IFIQ3023.doc', 5),
	# ('IHDT3023.doc', 5),
	# ('IUQR3043.doc', 5),
	# ('IUQR2234.doc', 3),
	# ('IUQR3064.doc', 5),
	# ('IUQR3164.doc', 5),
	# ('IUQR3054.doc', 5),
    # ('IUQR3083.doc', 5),
    # ('IUQR3074.doc', 5),
    # ('IUQR3183.doc', 5),
    # ('IUQR4104.doc', 4),
    # ('IUQR4124.doc', 5),
    # ('IUQR4093.doc', 5),
    # ('IUQR4114.doc', 5),
    # ('IUQR4134.doc', 5),
    # ('IUQR4154.doc', 4),
    # ('IUQR4144.doc', 5),
    ('IUQR4144-P0016.doc', 5),
]

def main():
    parser = argparse.ArgumentParser(description='Parse all MEDIU doc files in batch.')
    parser.add_argument("-l", '--log', dest='log', default="log.json",
                        help='JSON file used as log/database')
    args = parser.parse_args()

    process_json(args.log)

def process_json(log):
    data = log_read(log)
    if "processed" not in data:
        data["processed"] = []
        log_write(data, log)
    for fname, first_page in LIST:
        fname = os.path.abspath(fname)
        if fname in data["processed"]:
            continue
        while True:
            word = open_word()
            try:
                mediu_doc_parser.parse_doc(fname, first_page, log, word)
            except pywintypes.com_error as e:
                print("Caught pywintypes.com_error: ", e)
                time.sleep(3)
                try:
                    word.Documents.Close(SaveChanges=wdDoNotSaveChanges)
                    word.Quit()
                except Exception as e2:
                    print("Exception2: ", e2)
                    pass
                continue
            break

        # Perhaps there was some additional log processing
        data = log_read(log)
        data["processed"].append(fname)
        log_write(data, log)


if __name__ == "__main__":
    main()

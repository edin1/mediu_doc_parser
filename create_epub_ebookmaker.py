# coding=utf-8
import os
import argparse
import zipfile
import re
from collections import OrderedDict as OD
import json
import shutil

import bs4

import ebookmaker


STYLE = """
body {
font-family: "UthmanTN";
text-align: justify;
direction: rtl;
unicode-bidi: bidi-override;
}

h1 {
text-align: center;
font-family: "UthmanTN";
}
"""

# STYLE += """
# @font-face {
#   font-family: "AL-Hotham";
#   font-style: normal;
#   font-weight: normal;
#   src:url("al-hotham.ttf");
# }

# @font-face {
#   font-family: "AL-Mateen";
#   font-style: normal;
#   font-weight: normal;
#   src:url("AL-Mateen.ttf");
# }

# @font-face {
#   font-family: "AGA Rasheeq Bold";
#   font-style: normal;
#   font-weight: normal;
#   src:url("AGA Rasheeq Bold.ttf");
# }

# @font-face {
#   font-family: "AGA Arabesque";
#   font-style: normal;
#   font-weight: normal;
#   src:url("arabsq.ttf");
# }

# @font-face {
#   font-family: "AGA-Arabesque";
#   font-style: normal;
#   font-weight: normal;
#   src:url("arabsq.ttf");
# }

# @font-face {
#   font-family: "AGA Furat Regular";
#   font-style: normal;
#   font-weight: normal;
#   src:url("AGA.ttf");
# }

# @font-face {
#   font-family: "SC_ALYERMOOK";
#   font-style: normal;
#   font-weight: normal;
#   src:url("SC_ALYERMOOK.ttf");
# }


# p {
# font-family: "AL-Hotham";
# /*direction: rtl;
# unicode-bidi:bidi-override;*/
# }

# """

# STYLE = re.sub("src\:.*\;", 'src:url("uthmantn.otf");', STYLE)

STYLE += """
@font-face {
  font-family: "Hafs";
  font-style: normal;
  font-weight: normal;
  src: url("hafs-original.otf");
}

@font-face {
  font-family: "UthmanTN";
  font-style: normal;
  font-weight: normal;
  src:url("uthmantn.otf");
}

@font-face {
  font-family: "Amiri";
  font-style: normal;
  font-weight: normal;
  src:url("amiri-regular.ttf");
}

"""

# STYLE += """
# @font-face {
#   font-family: "Scheherazade";
#   font-style: normal;
#   font-weight: normal;
#   src:url("Scheherazade-Regular.ttf");
# }
# """

LANG = "ar"

FONT_EXTS = [".ttf", ".otf"]


def get_html_body(f):
    return bs4.BeautifulSoup(open(f, encoding="utf-8").read(), "lxml").find('body')


def process_html(f_full):
    f = os.path.basename(f_full)
    title_p = os.path.splitext(f)[0].split("-")[-1]
    title_p = str(int(title_p[1:]))
    with open(f_full, encoding="utf-8") as file:
        _html = file.read()
    _html = _html.replace('dir="LTR"', 'dir="rtl"')
    _html = _html.replace('dir=LTR', 'dir="rtl"')
    _html = _html.replace('RTL', 'rtl')
    soup = bs4.BeautifulSoup(_html, "lxml")
    if soup.head.title is None:
        soup.head.insert(0, soup.new_tag("title"))
    soup.head.title.string = title_p
    soup.body["lang"] = "ar-sa"
    soup.body.insert(0, soup.new_tag("h1", dir="ltr"))
    soup.body.h1.string = title_p
    soup.head.style.string = STYLE
    soup.html['xmlns'] = "http://www.w3.org/1999/xhtml"

    for span in soup.find_all(style=lambda c: c is not None and (c.find("AGA Arabesque") != -1)):
        if span.name != "span":
            print(span)
            raise Exception("Found a non-span tag with AGA Arabesque")
        # else:
        for c, r in [
                ("U", "ﷻ"),
                ("I", "ﷻ"),

                ("e", "ﷺ"),

                ("t", "(رضي الله عنه)"),
        ]:
            for span_sub in span.find_all(text=lambda s: s is not None and (s.strip() == c)):
                # span_sub.parent["style"] = span_sub.parent["style"].replace("font-size:20.0pt", "font-size:14.0pt")
                span_sub.parent.string = r

    for span in soup.find_all(style=lambda c: c is not None and (c.find("SC_ALYERMOOK") != -1)):
        if span.name != "span":
            print(span)
            raise Exception("Found a non-span tag with SC_ALYERMOOK")
        # else:
        for c, r in [
                ("#", "(عليه السلام)"),
                ("$", "(عليها السلام)"),
                ("&", "(عليهم السلام)"),
                ("^", "(عليهما السلام)"),

                (">", "(رضي الله عنه)"),
                ("<", "(رضي الله عنها)"),
                ("}", "(رضي الله عنهم)"),
                ("{", "(رضي الله عنهما)"),

                ("~", "(رحمه الله)"),

                ("@", "ﷺ"),
        ]:
            for span_sub in span.find_all(text=lambda s: s is not None and (s.strip() == c)):
                span_sub.parent.string = r

    for span in soup.find_all(style=lambda c: c is not None and (c.find("DecoType Thuluth") != -1)):
        if span.name != "span":
            print(span)
            raise Exception("Found a non-span tag with DecoType Thuluth")
        for c, r in [
                ("{", "﴿"),
                ("}", "﴾"),
                ("(", "﴿"),
                (")", "﴾"),
        ]:
            for span_sub in span.find_all(text=lambda s: s is not None and (s.strip() == c)):
                # span_sub.parent["style"] = span_sub.parent["style"].replace("font-size:20.0pt", "font-size:14.0pt")
                span_sub.parent.string = r

    for font in ("AGA Arabesque", "SC_ALYERMOOK"):
        for span in soup.find_all(style=lambda c: c is not None and (c.find(font) != -1)):
            style = " ".join(str(span["style"]).split())
            span["style"] = re.sub("size.*?pt", "size:16.0pt", style)
            # print(span.string)

    pattern = r"(color|colour):(\n|\s)*white"
    replacement = "color:white;background-color:navy"
    for span in soup.find_all(style=lambda c: c is not None and re.search(pattern, c)):
        if str(span.get_text()).strip() != "":
            # print(span)
            style = " ".join(str(span["style"]).split())
            span["style"] = re.sub(pattern, replacement, style)
        # print(span.string)

    _html_new = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">\n'
    _html_new += soup.prettify()
    _html_new = _html_new.replace('KFGQPC Uthmanic Script HAFS', "UthmanTN")
    # _html_new = _html_new.replace('"Hafs"', "Hafs")
    for font in [
            "AL-Hotham",
            "AL-Mateen",
            "AGA Rasheeq Bold",
            "AGA Furat Regular",
            "DecoType Thuluth",
            "Simplified Arabic",
            "Traditional Arabic",
    ]:
        # _html_new = _html_new.replace("'" + font + "'", "'UthmanTN'")
        _html_new = _html_new.replace('"' + font + '"', "UthmanTN")
        _html_new = _html_new.replace(font, "UthmanTN")
    # Replacing fonts for characters such as \uFDFB etc.
    for font in [
            "AGA Arabesque",
            "AGA-Arabesque",
            "SC_ALYERMOOK",
    ]:
        _html_new = _html_new.replace(font, "Amiri")
    # Replacing Hafs font special characters
    for c, r in [
            ("\u06E1", "ْ"),
            ("\u0656", "ٍ"),
            ("\u0657", "ً"),
            ("\u065E", "ٌ"),
            ("\u0671", "ا"),
            # إِبْرَٰهِ‍ۧمُ
            # إِبۡرَٰهِـۧمُ
    ]:
        _html_new = _html_new.replace(c, r)
    _html_new = re.sub(r"ِ‍ۧ(?![َٓ]?[\s$])", r"ِـۧ", _html_new)
    # import wordoff
    # print(wordoff.superClean(_html_new))
    with open(f_full, "w", encoding="utf-8") as file:
        file.write(_html_new)


def process_dir(d_src):
    d_src = os.path.abspath(d_src)
    d_script = os.path.dirname(os.path.abspath(__file__))
    d = d_src + "_epub"
    if os.path.exists(d):
        shutil.rmtree(d)
    shutil.copytree(d_src, d)
    ebookmaker_json = OD()
    files = os.listdir(d)
    files.sort()
    files_html = []
    files_fonts = []
    for f in files:
        f_full = os.path.join(d, f)
        if os.path.isdir(f):
            continue
        else:
            ext = os.path.splitext(f.lower())[1].lower()
            if ext in (".html", ".htm"):
                process_html(f_full)
                files_html.append(f_full)
            # elif ext in FONT_EXTS:
            #     files_fonts.append(f_full)
    for font in [
            "amiri-regular.ttf",
            "uthmantn.otf",
            "hafs.otf",
            "hafs-original.otf",
    ]:
        files_fonts.append(os.path.join(d_script, font))
    # print(files_fonts)
    if not files_html:
        raise Exception("There are no html files in the directory you provided")
    # else:

    title = os.path.basename(files_html[0]).rsplit("-", 1)[0]

    ebookmaker_json["filename"] = ebookmaker_json["title"] = title
    ebookmaker_json["language"] = "ar"
    ebookmaker_json["contents"] = [
        {
            "type" : "toc",
            "source" : "toc.html"
        },
        {
            "type": "text",
            "source": "%s*.htm*"%title
        },
    ]

    ebookmaker_json["authors"] = [
        {
            "name" : "some author",
            "sort" : "author"
        }
    ]

    ebookmaker_json["rights"]  = "MEDIU"
    ebookmaker_json["publisher"]  = "MEDIU"

    ebookmaker_json["identifier"] = {
        "scheme" : "URL",
        "value" : "http://www.mediu.edu.my"
    }

    ebookmaker_json["toc"] = {
        "depth" : 3,
        "parse" : ["text"],
        "generate" : {
            "title" : "الصفحات"
        }
    }

    ebookmaker_json["guide"] = [
        {
            "type" : "toc",
            "title" : "الصفحات",
            "href" : "toc.html"
        },
        {
            "type" : "text",
            "title" : "البداية",
            "href" : files_html[0]
        }
]

    file_json = os.path.join(d, title + ".json")
    with open(file_json, "w", encoding="utf-8") as _f:
        json.dump(ebookmaker_json, _f, indent=2, ensure_ascii=False)

    with open(os.path.join(d, "style.css"), "w", encoding="utf-8") as _f:
        _f.write(STYLE)

    shutil.copytree(os.path.join(d_script, "opf-templates"), os.path.join(d, "opf-templates"))
    cwd_old = os.path.abspath(os.getcwd())
    os.chdir(d)
    gen = ebookmaker.OPFGenerator(ebookmaker.parseEBookFile(file_json))
    gen.createEBookFile()
    os.chdir(cwd_old)

    # Add fonts
    if files_fonts:
        z = zipfile.ZipFile(os.path.join(d, title + ".epub"), "a")
        for f_full in files_fonts:
            f = os.path.basename(f_full)
            z.write(f_full, os.path.join("OEBPS", f))
            shutil.copyfile(f_full, os.path.join(d, f))
            # ext = os.path.extsep(f)[1].lower()
            # if ext in [".ttf"]:
            #     media_type = "application/x-font-truetype"
            # else:
            #     raise Exception("Unsupported font type")
            # book.add_item(file_name=f, content=open(f_full, "rb").read(), media_type=media_type)
        z.close()


def change_fonts_in_dir(path):
    path_new = path + '_fonts'
    try:
        os.mkdir(path_new)
    except FileExistsError:
        pass

    files = os.listdir(path)
    files.sort()
    for file in files:
        if not file.endswith('.html'):
            continue
        text = open(os.path.join(path, file), encoding='utf-8').read()
        # text = text.replace('KFGQPC HAFS Uthmanic Script', 'Hafs')
        text = text.replace('KFGQPC HAFS Uthmanic Script', 'UthmanTN')
        text = text.replace('KFGQPC Uthman Taha Naskh', 'UthmanTN')
        with open(os.path.join(path_new, file), 'w', encoding='utf-8') as f: f.write(text)
    return path_new


def main():
    parser = argparse.ArgumentParser(description='Create epub from all html files in a directory.')
    parser.add_argument('path', default=".",
                    help='The file or directory path where to look for html files.')

    args = parser.parse_args()
    path = os.path.abspath(args.path)
    if os.path.isdir(path):
        path_new = change_fonts_in_dir(path)
        process_dir(path_new)
    else:
        raise Exception("You have to supply a valid directory.")


if __name__ == '__main__':
    main()

import sys
import os
import re
import argparse
import copy

import bs4

import create_epub_ebookmaker

PATTERN = 'QCF_P[0-9]+'

PATTERNC = re.compile(PATTERN)


def main():
    parser = argparse.ArgumentParser(description='Clean up MEDIU html files in a directory.')
    parser.add_argument("directory")
    args = parser.parse_args()

    directory_new = process_dir(args.directory)
    directory_new2 = create_epub_ebookmaker.change_fonts_in_dir(directory_new)
    create_epub_ebookmaker.process_dir(directory_new2)


def process_dir(directory):
    # print(directory)
    directory_new = directory + '_cleaned'
    try:
        os.mkdir(directory_new)
    except FileExistsError:
        pass
    files = os.listdir(directory)
    files.sort()
    # print(files)
    for i, file in enumerate(files):
        file_old = os.path.join(directory, file)
        file_new = os.path.join(directory_new, file)
        with open(file_old, encoding='utf-8') as f:
            text_new = f.read()

        # Adding page navigation
        basename, extension = os.path.splitext(file)
        page_number = int(basename[-4:])
        print('Processing', i, 'page:', page_number)
        basename_basis = basename[:-4]
        # print(basename_basis)
        if i == 0:
            previous_page = '<del>السابق</del>'
            first_page_number = page_number
            last_page_number = first_page_number + len(files) - 1
            first_page = '<del>البداية</del>'
        else:
            previous_page = """<a href="%s%04d%s">السابق</a>""" % (basename_basis, page_number - 1, extension)
            first_page = """<a href="%s%04d%s">البداية</a>""" % (basename_basis, first_page_number, extension)

        if i + 1 == len(files):
            next_page = '<del>التالي</del>'
            last_page = '<del>النهاية</del>'
        else:
            next_page = """<a href="%s%04d%s">التالي</a>""" % (basename_basis, page_number + 1, extension)
            last_page = """<a href="%s%04d%s">النهاية</a>""" % (basename_basis, last_page_number, extension)

        text_new = PATTERNC.sub('KFGQPC HAFS Uthmanic Script', text_new)
        # text_new = re.sub(r"""</span><span\s+lang=AR-SA\s+style='font-size:15.0pt;color:green'>""", '', text_new)
        # text_new = re.sub(r"""</span><span\s+lang=AR-SA\s+style="font-size:15.0pt;color:green">""", '', text_new)

        # text_new = re.sub(r"""</span><span\s+lang=AR-SA\s+style='font-size\s*:\s*15.0pt\s*;\s*font-family\s*:\s*"Tahoma"\s*,\s*"sans-serif"\s*;\s*color\s*:\s*green'\s*>""", '', text_new)
        # text_new = re.sub(r"""</span><span\s+lang=AR-SA\s+style="font-size\s*:\s*15.0pt\s*;\s*font-family\s*:\s*"Tahoma"\s*,\s*"sans-serif"\s*;\s*color\s*:\s*green"\s*>""", '', text_new)

        text_new = re.sub(r"""font-family:\s+""", 'font-family:', text_new)
        text_new = re.sub(r"""font-size:\s+""", 'font-size:', text_new)
        text_new = re.sub(r""";\s+color:green""", ';color:green', text_new)

        text_new = text_new.replace("""AL-Hotham""", 'KFGQPC Uthman Taha Naskh')
        # text_new = text_new.replace("""""", '')

#         text_new = text_new.replace("""</span><span
# lang=AR-SA style='font-size:15.0pt;color:green'>""", '')
#         text_new = text_new.replace("""</span><span lang=AR-SA
# style='font-size:15.0pt;font-family:"Tahoma","sans-serif";color:green'>""", '')
#         text_new = text_new.replace("""</span><span
# lang=AR-SA style='font-size:15.0pt;font-family:"Tahoma","sans-serif";
# color:green'>""", '')
#         text_new = text_new.replace("""</span><span lang=AR-SA style='font-size:15.0pt;
# font-family:"Tahoma","sans-serif";color:green'>""", '')
#         text_new = text_new.replace("""</span><span lang=AR-SA style='font-size:15.0pt;font-family:
# "Tahoma","sans-serif";color:green'>""", '')
#         text_new = text_new.replace("""""", '')
#         text_new = text_new.replace('ۡ', 'ْ')

        soup = bs4.BeautifulSoup(text_new, 'lxml')

        processed = 0
        while True:
            # print('span', processed)
            try:
                span = soup.find_all('span', style=lambda value: value and 'Uthmanic' in value)[processed]
            except IndexError:
                # print('Fixed all HAFS')
                break
            next = span.next_sibling
            nexts_to_decompose = []
            while next and (next.has_attr('style') and ('Thuluth' not in next['style']) and (('color:green' in next['style']) or ('Uthmanic' in next['style']) or ('font-family' not in next['style']) or ('Tahoma' in next['style']))):
            # while next and ((not next.has_attr('style')) or ((next.has_attr('style')) and ('Thuluth' not in next['style']) and (('color:green' in next['style']) or ('Uthmanic' in next['style']) or ('font-family' not in next['style']) or ('Tahoma' in next['style'])))):
                if next.text:
                    if span.string is None:
                        span.string = span.text
                    span.string += next.text
                nexts_to_decompose.append(next)
                next = next.next_sibling
            for next in nexts_to_decompose:
                next.decompose()
            processed += 1

        # For creating a single html
        if i == 0:
            _text = str(soup)
            # print(_text)
            _text = re.sub("""(<body.*>)""", r'\1<div class="mypagination" style="text-align:center;font-weight:bold">صفحة %d</div>' % page_number, _text)
            # print(_text)

            soup_all_pages_with_page_numbers = bs4.BeautifulSoup(_text, 'lxml')
            soup_all_pages_without_page_numbers = bs4.BeautifulSoup(_text, 'lxml')
        else:
            tag = soup_all_pages_with_page_numbers.new_tag('div', style="text-align:center;font-weight:bold")
            tag['class'] = 'mypagination'
            tag.string = 'صفحة ' + str(page_number)
            soup_all_pages_with_page_numbers.body.append(tag)
            for element in soup.find('body'):
                soup_all_pages_with_page_numbers.body.append(copy.copy(element))
                soup_all_pages_without_page_numbers.body.append(copy.copy(element))
        with open(file_new, 'w', encoding='utf-8') as f:
            text_new = str(soup)
            for pattern in (r"""(<body.*>)""", r"""(</body>)"""):
                text_new = re.sub(pattern, r'\1<div class="mynavigation" style="text-align:center;">%s | %s | %s | %s </div>' % (first_page, previous_page, next_page, last_page), text_new)
            f.write(text_new)
    with open(directory_new + '-with-page-numbers.html', 'w', encoding='utf-8') as f:
        f.write(str(soup_all_pages_with_page_numbers))
    with open(directory_new + '-without-page-numbers.html', 'w', encoding='utf-8') as f:
        f.write(str(soup_all_pages_without_page_numbers))
    return directory_new


if __name__ == "__main__":
    main()

import sys
from helpers import *
word = open_word()
word.Visible = True

def main():
    parser = argparse.ArgumentParser(description='Find word files with more than n pages')
    parser.add_argument('path', default=".",
                    help='The file or directory path where to replace fonts.')
    parser.add_argument("-n", '--number', dest='number', type=int, default=1,
                        help='The number of pages the file should have more than')

    args = parser.parse_args()
    path = os.path.abspath(args.path)
    if os.path.isdir(path):
        process_dir(path)
    else:
        process_file(path)

def process_dir(d):
    files = os.listdir(d)
    files.sort()
    for f in files:
        f_full = os.path.join(d, f)
        if os.path.isdir(f):
            # print('"%s" is a dir, recursing into it...'%f_full)
            process_dir(f_full)
        elif os.path.splitext(f.lower())[1].lower() in (".doc", ".docx"):
            process_file(f_full)

def process_file(f):
    if not os.path.isfile(f) and os.path.splitext(f.lower())[1].lower() not in (".doc", ".docx"):
        warning('"%s" is not a Word document.'%f)
        return
    # else:
    word.Documents.Open(f)
    go_to_end(word)
    # go_to_beginning(word)
    loop_through_shapes(word)
    # word.ActiveDocument.Save()
    # word.Quit()
    # replace_Quran_fonts(word)

def loop_through_shapes(word):
    convert_shapes_to_text(word)
    # for shape in word.ActiveDocument.Shapes:
    #     print(shape.TextFrame.TextRange.Text)
    #     print(shape.Type)

if __name__ == "__main__":
    main()

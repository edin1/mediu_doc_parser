import sys
from helpers import *
word = open_word()
word.Visible = True

def main():
    parser = argparse.ArgumentParser(description='Find word files with more than n pages')
    parser.add_argument('path', default=".",
                    help='The file or directory path where to replace fonts.')
    parser.add_argument("-n", '--number', dest='number', type=int, default=1,
                        help='The number of pages the file should have more than')

    args = parser.parse_args()
    path = os.path.abspath(args.path)
    if os.path.isdir(path):
        process_dir(path)
    else:
        process_file(path)

def process_dir(d):
    files = os.listdir(d)
    files.sort()
    for f in files:
        f_full = os.path.join(d, f)
        if os.path.isdir(f):
            # print('"%s" is a dir, recursing into it...'%f_full)
            process_dir(f_full, n)
        elif os.path.splitext(f.lower())[1].lower() in (".doc", ".docx"):
            process_file(f_full)

def process_file(f):
    if not os.path.isfile(f) and os.path.splitext(f.lower())[1].lower() not in (".doc", ".docx"):
        warning('"%s" is not a Word document.'%f)
        return
    # else:
    word.Documents.Open(f)
    fix_margins()
    # word.ActiveDocument.Close()

if __name__ == "__main__":
    main()

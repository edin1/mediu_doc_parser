import sys
from helpers import *
word = open_word()
word.Visible = True

def main():
    parser = argparse.ArgumentParser(description='Build MEDIU epub from all html files in a directory')
    parser.add_argument('path', default=".",
                    help='The directory path for html files.')

    args = parser.parse_args()
    path = os.path.abspath(args.path)
    process_dir(path)

def process_dir(d):
    files = os.listdir(d)
    files.sort()
    for f in files:
        f_full = os.path.join(d, f)
        if os.path.isdir(f):
            # print('"%s" is a dir, recursing into it...'%f_full)
            process_dir(f_full)
        elif os.path.splitext(f.lower())[1].lower() in (".doc", ".docx"):
            process_file(f_full)

def process_file(f):
    if not os.path.isfile(f) and os.path.splitext(f.lower())[1].lower() not in (".doc", ".docx"):
        warning('"%s" is not a Word document.'%f)
        return
    # else:
    word.Documents.Open(f)
    go_to_beginning(word)
    replace_all_fonts(word)
    # word.ActiveDocument.Save()
    # word.Quit()
    # replace_Quran_fonts(word)

def loop_through_words(word):
    for sentence in word.ActiveDocument.StoryRanges:
        for w in sentence.Words:
            #w.Font.Name = "Scheherazade"
            if w.Text == '68': # and w.Font.Name == "Times New Roman":
                print(w.Text)
                print(w.Font.Name)
                # w.Font.Name = "AL-Hotham"
                # print("ﷺ")

def loop_through_paragraphs(word):
    for par in word.ActiveDocument.Paragraphs:
        #par.Font.Name = "Scheherazade"
        print(par.Font.Name)

def replace_fonts(word):
    # word.ActiveDocument.Content.Select()
    find = word.Selection.Find

    find.ClearFormatting()
    find.Font.Name = "AGA Rasheeq Bold"
    find.Font.Name = "Times New Roman"
    find.Text = ""

    find.Forward = True
    find.Wrap = wdFindContinue
    find.Format = True
    find.MatchCase = False
    find.MatchWholeWord = False
    find.MatchWildcards = False
    find.MatchSoundsLike = False
    find.MatchAllWordForms = False

    find.Replacement.ClearFormatting()
    find.Replacement.Font.Name = "Scheherazade"
    find.Replacement.Text = ""

    find.Execute(Replace=wdReplaceAll)
    # word.ActiveDocument.Close()


if __name__ == "__main__":
    main()

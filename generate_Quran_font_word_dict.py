import os
import sqlite3
import html
from collections import OrderedDict as OD
import re

STOP_SIGNS = ["ۛ", "ۖ", "ۗ", "ۚ", "ۙ", "ۘ"]#, "ۜ"]
SAJDA = "۩"
RUB = "۞"
SPECIAL_SIGNS = STOP_SIGNS + [SAJDA, RUB]

def create_db(fname, table, columns, rows):
    if isinstance(fname, sqlite3.Connection):
        con = fname
    else:
        con = sqlite3.connect(fname)
    con.execute("DROP TABLE IF EXISTS %s"%table)
    _columns = "\n,".join("%s %s"%(col[0], col[1]) for col in columns)
    cmd_create = """
        create table IF NOT EXISTS %s(
        %s
        );

        """%(table, _columns)
    con.execute(cmd_create)
    _columns = ", ".join(col[0] for col in columns)
    cmd_insert ="""
        INSERT INTO %s
        (%s)
        VALUES
        (%s)"""%(table, _columns, ", ".join(["?"]*len(columns)))
    for row in rows:
        con.execute(cmd_insert, row)
    con.commit()


fname_scanned = "text.sqlite3.db"
fname_plain_text = "hafs_qurancomplex_pages.sqlite"

con_scanned = sqlite3.connect(fname_scanned)
con_plain_text = sqlite3.connect(fname_plain_text)

columns = ["page", "line", "sura", "ayah", "text"]
ci = columns.index

sql = """
SELECT {columns}
FROM %s
ORDER BY page, line;
""".format(columns=", ".join(columns))

rows_scanned = list(con_scanned.execute(sql%"madani_page_text"))

rows_plain_text = list(con_plain_text.execute(sql%os.path.splitext(fname_plain_text)[0]))

assert len(rows_scanned) == len(rows_plain_text)

word_list = []
for row_scanned, row_plain_text in zip(rows_scanned, rows_plain_text):
    ayah = row_scanned[ci("ayah")]
    if ayah is None or ayah < 1:
        continue
    for c in columns[:-1]:
        locals()[c] = row_scanned[ci(c)]
        if row_scanned[ci(c)] != row_plain_text[ci(c)]:
            print(row_scanned)
            print(row_plain_text)
            raise Exception("Line data mismatch")
    text_scanned_html = row_scanned[ci("text")]
    text_scanned = html.unescape(text_scanned_html)

    text_plain_text = row_plain_text[ci("text")]
    # Remove stop sign that's missing from the scanned text
    if (page, line) in [
            (12, 8),
            (17, 14),
            (59, 12),
            (254, 6),
            (273, 10),
            (380, 2),
            (395, 7),
            (479, 2),
            (511, 15),
            (541, 8),
            (589, 10),
    ]:
        text_plain_text = text_plain_text.replace("ۚ", "")
    if (page, line) in [
            (20, 12),
            (34, 14),
            (62, 6),
            (63, 14),
            (64, 14),
            (73, 7),
            (88, 3),
            (95, 2),
            (157, 7),
            (229, 3),
            (229, 4),
            (268, 14),
            (303, 8),
            (335, 12),
            (336, 3),
            (339, 7),
            (369, 15),
            (389, 2),
            (397, 7),
            (400, 5),
            (414, 2),
            (436, 5),
            (467, 2),
            (467, 14),
            (507, 8),
            (539, 1),
            (539, 4),
            (541, 7),
            (543, 6),
            (559, 14),
            (566, 14),
            (571, 15),
            (593, 13),
    ]:

        text_plain_text = text_plain_text.replace("ۖ", "")
    # Special treatment
    if (page, line) == (270, 4):
        text_plain_text = text_plain_text[:-1]
    # if (page, line) == (293, 12):
    #     text_plain_text = text_plain_text.replace('عِوَجَاۜ', 'عِوَجَا ۜ')
    if (page, line) == (337, 10):
        text_plain_text = text_plain_text.replace('مُوسَىٰۖ', 'مُوسَىٰ')
    if (page, line) == (403, 1):
        text_plain_text = text_plain_text[:-1]
    if (page, line) == (432, 5):
        text_plain_text = text_plain_text.replace('ٱلۡعَذَابَۚ', 'ٱلۡعَذَابَ')
    if (page, line) == (506, 9):
        text_plain_text = text_plain_text[:-1]
    for s in SPECIAL_SIGNS:
        text_plain_text = text_plain_text.replace(s, " " + s)
    text_plain_text = re.sub(r"ۜ( |$|[٠١٢٣٤٥٦٧٨٩])", r" ۜ\1", text_plain_text)
    text_plain_text = text_plain_text.strip()
    text_plain_text = text_plain_text.replace("  ", " ")
    text_plain_text_list = text_plain_text.split(" ")
    if len(text_scanned) != len(text_plain_text_list):
        print(page, line, sura, ayah)
        print(len(text_scanned))
        # print(text_scanned)
        for c in text_scanned:
            print(c)
        print(len(text_plain_text.split(" ")))
        # print(text_plain_text)
        print(text_plain_text.split(" "))
        raise Exception("Differing number of words")
    else:
        for i, c in enumerate(text_scanned):
            word_list.append(("QCF_P%03d.TTF"%page, c, i+1, line, sura, ayah, text_plain_text_list[i]))

name = "Quran_scanned_symbols_to_text"
columns_new =[
    ("font", "TEXT"),
    ("character", "TEXT"),
    ("index_in_line", "INTEGER"),
    ("line", "INTEGER"),
    ("sura", "INTEGER"),
    ("ayah", "INTEGER"),
    ("text", "TEXT"),
]
create_db(name + ".sqlite", name, columns_new, word_list)
    # print(row_scanned, row_plain_text)

import os
import time
import argparse
import shutil
import sqlite3
import json
import collections

import win32com
import win32com.client
import win32gui
import pywintypes
import colorama
colorama.init()

# Click on WdBrowseTarget at:
# https://msdn.microsoft.com/en-us/library/office/aa211923(v=office.11).aspx
# This was also handy:
# http://groovy.jmiguel.eu/groovy.codehaus.org/modules/scriptom/1.6.0/scriptom-office-2K3-tlb/apidocs/org/codehaus/groovy/scriptom/tlb/office/word/WdBrowseTarget.html
wdBrowsePage = 1
wdGoToFirst = 1
wdGoToSection = 0
wdAlignParagraphCenter = 1
wdReplaceAll = 2
wdCharacter = 1
wdExtend = 1
wdMainTextStory = 1
wdHeaderFooterPrimary = 1
wdHeaderFooterFirstPage = 2
wdHeaderFooterEvenPages = 3
wdFieldEmpty = -1
wdFindContinue = 1
wdOrientPortrait = 0
wdSectionContinuous = 0
wdAlignVerticalTop = 0
wdGutterPosLeft = 0
wdSectionDirectionRtl = 0
wdFormatFilteredHTML = 10
msoEncodingUTF8 = 65001
# Shapes
msoAutoShape = 1
msoTextBox = 17

wdActiveEndAdjustedPageNumber = 1
wdActiveEndPageNumber = 3

wdDoNotSaveChanges = 0

GREEN = '\033[92m'
RED = '\033[91m'
ENDC = '\033[0m'


QURAN_FONT_TABLE = "Quran_scanned_symbols_to_text"
QURAN_DB = sqlite3.connect(QURAN_FONT_TABLE + ".sqlite")

def warning(s):
    print(RED + s + ENDC)

def ok(s):
    print(GREEN + s + ENDC)

def open_word():
    word = win32com.client.gencache.EnsureDispatch('Word.Application')
    #the Microsoft Word application would launch and grab focus if not set to False
    word.Visible = True
    return word

def go_to_end(word):
    # http://word.tips.net/T000826_Moving_to_the_Start_or_End_of_the_Real_Document.html
    word.ActiveDocument.Characters.Last.Select()
    word.Selection.Collapse()
    # print("Waiting %ds for document to be loaded..."%WAIT)
    # time.sleep(WAIT)

def go_to_beginning(word):
    # Making sure we're at the beginning of the document (from same source)
    word.Selection.GoTo(What=wdGoToSection, Which=wdGoToFirst)

def remove_empty_paragraphs(word):
    replace(word, "(^13){2,}", "^p")
    # replace(word, "^l", "")
    # if word.ActiveDocument.Paragraphs.Last.Range.Text.strip() == "":
    #     word.ActiveDocument.Paragraphs.Last.Range.Delete()
    # go_to_end(word)
    # word.Selection.MoveLeft(Unit=wdCharacter, Count=1, Extend=wdExtend)
    # print(word.Selection.Text)
    # if word.Selection.Text == "^p":
    #     word.Selection.Delete(Unit=wdCharacter, Count=1)

def remove_breaks(word):
    # Broken!!!
    for b in ("^m", "^b"):
        replace(word, b, "")

def remove_empty_at_end(word):
    go_to_end(word)
    # As long as the last character is a carriage return [CHR(13)]...
    word.Selection.MoveLeft(Unit=wdCharacter, Count=1, Extend=wdExtend)
    while word.Selection.Text in (chr(13), chr(2), chr(10), chr(12), chr(14)):
        # ... Delete the character, and select the new last character
        word.Selection.Delete(Unit=wdCharacter, Count=1)
        if word.ActiveDocument.Range().Text == "\r":
            break
        word.Selection.MoveLeft(Unit=wdCharacter, Count=1, Extend=wdExtend)
    # Go to the end of the file again to not leave a character selected
    go_to_beginning(word)

def replace(word, text_find, text_replace):
    word.ActiveDocument.Content.Select()
    word.Selection.Find.ClearFormatting()
    word.Selection.Find.Replacement.ClearFormatting()
    word.Selection.Find.MatchWildcards = False
    word.Selection.Find.Text = text_find
    word.Selection.Find.Replacement.Text = text_replace
    word.Selection.Find.Execute(Replace=wdReplaceAll)

def InchesToPoints(i):
    return i*72

def fix_margins(word):
    go_to_beginning(word)
    p = word.Selection.PageSetup
    p.LineNumbering.Active = False
    p.Orientation = wdOrientPortrait
    p.TopMargin = InchesToPoints(1)
    p.BottomMargin = InchesToPoints(1)
    p.LeftMargin = InchesToPoints(1)
    p.RightMargin = InchesToPoints(1)
    p.Gutter = InchesToPoints(0)
    p.HeaderDistance = InchesToPoints(0.49)
    p.FooterDistance = InchesToPoints(0.49)
    p.PageWidth = InchesToPoints(8.27)
    p.PageHeight = InchesToPoints(11.69)
    # p.FirstPageTray = wdPrinterDefaultBin
    # p.OtherPagesTray = wdPrinterDefaultBin
    p.SectionStart = wdSectionContinuous
    p.OddAndEvenPagesHeaderFooter = True
    p.DifferentFirstPageHeaderFooter = True
    p.VerticalAlignment = wdAlignVerticalTop
    p.SuppressEndnotes = False
    p.MirrorMargins = False
    p.TwoPagesOnOne = False
    p.BookFoldPrinting = False
    p.BookFoldRevPrinting = False
    p.BookFoldPrintingSheets = 1
    p.GutterPos = wdGutterPosLeft
    p.SectionDirection = wdSectionDirectionRtl

def save_filtered_html(word, fname):
    # Uncomment if needed
    # word.ActiveDocument.Fields.Update()
    word.ActiveDocument.WebOptions.Encoding = msoEncodingUTF8
    word.ActiveDocument.SaveAs2(FileName=fname, FileFormat=wdFormatFilteredHTML, LockComments=False, Password="", AddToRecentFiles=True, WritePassword="", ReadOnlyRecommended=False, EmbedTrueTypeFonts=False, SaveNativePictureFormat=False, SaveFormsData=False,SaveAsAOCELetter=False, CompatibilityMode=0)

def replace_all_fonts(word):
    for sentence in word.ActiveDocument.StoryRanges:
        # print(sentence)
        # print()
        # print()
        replace_all_fonts_in_words(sentence.Words)


def replace_all_fonts_in_words(words):
    sql = """
SELECT text
FROM %s
WHERE font='%s' and character='%s';
"""
    for j in range(words.Count, 0, -1):
        w = words(j)
        # print(w.Font.Name)
        # print(w.Text)
        # print(repr(w.Text))
        if w.Font.Name.lower() == "qcf_bsml":
            text_new = w.Text
            for c, r in [
                    ("(", "{"),
            ]:
                text_new = text_new.replace(c, r)
            w.Font.Name = "DecoType Thuluth"
            w.Text = text_new
        elif w.Font.Name.lower().startswith("qcf"):
            text_new = ""
            for c in w.Text:
                if c in [" ", "\r"]:
                    text_new += c
                else:
                    font = "%s.TTF"%w.Font.Name.upper()
                    # print(type(c), repr(c))
                    rows = list(QURAN_DB.execute(sql%(QURAN_FONT_TABLE, font, c)))
                    if len(rows) != 1:
                        msg = "The corresponding Qur'anic text was not found"
                        print(rows)
                        print(repr(font), repr(c))
                        # print(msg)
                        # continue
                        raise Exception(msg)
                    text_new += rows[0][0]
            w.Font.Name = "KFGQPC Uthmanic Script HAFS"
            w.Text = text_new
        # else:
            # print(w.Font.Name)
            # print(w)
        # elif w.Font.Name in ("AGA Arabesque", "Times New Roman", ""):
        #     text = w.Text
        #     for c, r in [
        #             ("U", "ﷻ"),
        #             ("I", "ﷻ"),
        #             ("", "ﷻ"),
        #             ("", "ﷻ"),
        #             ("", "ﷻ"),

        #             ("(", "ﷺ"),
        #             ("e", "ﷺ"),
        #             ("", "ﷺ"),
        #             ("", "ﷺ"),

        #             ("#", "(عليه السلام)"),
        #             ("", "(عليه السلام)"),

        #             (">", "(رضي الله عنه)"),
        #             ("", "(رضي الله عنه)"),
        #             ("", "(رضي الله عنهم)"),
        #     ]:
        #         text = text.replace(c, r)
        #     w.Select()
        #     w.Delete()
        #     word.Selection.TypeText(text)
        #     # w.Font.Name = "Amiri"
        #     # w.Font.Size = 12
        #     # w.Text = text
        # elif w.Font.Name == "SC_ALYERMOOK":
        #     text = w.Text
        #     for c, r in [
        #             ("#", "(عليه السلام)"),
        #             ("$", "(عليها السلام)"),
        #             ("&", "(عليهم السلام)"),
        #             ("^", "(عليهما السلام)"),

        #             ("<", "(رضي الله عنه)"),
        #             (">", "(رضي الله عنها)"),
        #             ("{", "(رضي الله عنهم)"),
        #             ("}", "(رضي الله عنهما)"),

        #             ("<", "(رضي الله عنه‎‎)"),
        #             (">", "(رضي الله عنه‎‎ا)"),
        #             ("{", "(رضي الله عنه‎‎م)"),
        #             ("}", "(رضي الله عنه‎‎ما)"),

        #             ("~", "(رحمه الله)"),

        #             ("@", "ﷺ"),

        #     ]:
        #         text = text.replace(c, r)
        #     w.Select()
        #     w.Delete()
        #     word.Selection.TypeText(text)
        #     # w.Font.Name = "Amiri"
        #     # w.Font.Size = 12
        #     # w.Text = text


def convert_shapes_to_text(word):
    # print(n)
    # return
    # print(word.ActiveDocument.Shapes.Count)
    for i in range(word.ActiveDocument.Shapes.Count, 0, -1):
        shape = word.ActiveDocument.Shapes(i)
        # if shape.Type in (msoTextBox, msoAutoShape):
        #   text = shape.TextFrame.TextRange.Text
        #   ILS = shape.ConvertToInlineShape()
        #   rng = ILS.Range
        #   rng.Text = text
        # else:
        #     raise Exception()
        shape.Select()
        # print(shape.Type)
        # print(shape.TextFrame.HasText)
        # print(word.Selection.Information(wdActiveEndAdjustedPageNumber))
        if shape.TextFrame.HasText == -1: # -1 seems to be true
            shape.Select()
            replace_all_fonts_in_words(shape.TextFrame.TextRange.Words)
            # print(shape.GroupItems)
            text = shape.TextFrame.TextRange.Text
            # print(repr(text))
            # print(repr(shape.TextFrame.TextRange.Font.Name))
            # print(repr(shape.TextFrame.TextRange.Words))
            shape.Delete()
            word.Selection.TypeText(text)

def dir_create(path):
    if not os.path.exists(path):
        try:
            os.makedirs(path)
        except OSError as err:
            if err.errno!=17:
                raise

def log_write(data, fname):
    # print(data, type(data))
    with open(fname, "w", encoding="utf-8") as f:
        json.dump(data, f, indent=2)

def log_read(fname):
    if not os.path.exists(fname):
        return {}
    with open(fname, encoding="utf-8") as f:
        data = json.load(f)
    return data

# def OD(data):
#     if isinstance(data, list):
#         return [OD(e) for e in data]
#     elif isinstance(data, (dict, collections.OrderedDict)):
#         res = collections.OrderedDict()
#         keys = list(data.keys())
#         keys.sort()
#         for key in keys:
#             res[key] = OD(data[key])
#         return res
#     else:
#         return data

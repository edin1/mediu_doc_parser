# coding=utf-8
import os
import argparse
import zipfile
import re

from ebooklib import epub

STYLE = """
BODY { text-align: justify;}

@font-face {
  font-family: "AL-Hotham";
  font-style: normal;
  font-weight: normal;
  src:url("fonts/al-hotham.ttf");
}

@font-face {
  font-family: "AL-Mateen";
  font-style: normal;
  font-weight: normal;
  src:url("fonts/AL-Mateen.ttf");
}

@font-face {
  font-family: "AGA Rasheeq Bold";
  font-style: normal;
  font-weight: normal;
  src:url("fonts/AGA Rasheeq Bold.ttf");
}

@font-face {
  font-family: "AGA Arabesque";
  font-style: normal;
  font-weight: normal;
  src:url("fonts/arabsq.ttf");
}

@font-face {
  font-family: "AGA-Arabesque";
  font-style: normal;
  font-weight: normal;
  src:url("fonts/arabsq.ttf");
}

p {
font-family: "AL-Hotham";
/*direction: rtl;
unicode-bidi:bidi-override;*/
}

"""

STYLE = re.sub("src\:.*\;", 'src:url("fonts/Scheherazade-Regular.ttf");', STYLE)

DEFAULT_CSS = epub.EpubItem(uid="style_default", file_name="style/default.css", media_type="text/css", content=STYLE)

LANG = "ar"

FONT_EXTS = [".ttf",]


def main():
    parser = argparse.ArgumentParser(description='Create epub from all html files in a directory.')
    parser.add_argument('path', default=".",
                    help='The file or directory path where to look for html files.')

    args = parser.parse_args()
    path = os.path.abspath(args.path)
    if os.path.isdir(path):
        process_dir(path)
    else:
        raise Exception("You have to supply a valid directory.")

def process_dir(d):
    files = os.listdir(d)
    files.sort()
    files_html = []
    files_fonts = []
    for f in files:
        f_full = os.path.abspath(os.path.join(d, f))
        if os.path.isdir(f):
            continue
        else:
            ext = os.path.splitext(f.lower())[1].lower()
            if ext in (".html", ".htm"):
                files_html.append(f_full)
            elif ext in FONT_EXTS:
                files_fonts.append(f_full)
    if not files_html:
        raise Exception("There are no html files in the directory you provided")
    # else:
    book = epub.EpubBook()
    title = os.path.basename(files_html[0]).rsplit("-", 1)[0]
    book.set_title(title)
    book.set_language(LANG)
    book.add_item(DEFAULT_CSS)

    pages = []
    for f_full in files_html:
        f = os.path.basename(f_full)
        # page number for now
        title_p = os.path.splitext(f)[0].split("-")[-1]
        # remove the P letter
        title_p = str(int(title_p[1:]))
        p = epub.EpubHtml(title=title_p, file_name=f, lang=LANG, content=open(f_full, "rb").read())
        p.add_item(DEFAULT_CSS)
        book.add_item(p)
        pages.append(p)
    book.toc = ((epub.Section("Pages"), pages),)
    book.add_item(epub.EpubNcx())
    book.add_item(epub.EpubNav())

    style_nav = '''
@namespace epub "http://www.idpf.org/2007/ops";


nav[epub|type~='toc'] > ul > li > ul  {
    list-style-type:square;
}


nav[epub|type~='toc'] > ul > li > ul > li {
        margin-top: 0.3em;
}

'''

    # add css file
    nav_css = epub.EpubItem(uid="style_nav", file_name="style/nav.css", media_type="text/css", content=style_nav)
    book.add_item(nav_css)
    book.spine = ['nav'] + pages

    epub.write_epub(title + ".epub", book, {})

    # Add fonts
    if files_fonts:
        z = zipfile.ZipFile(title + ".epub", "a")
        for f_full in files_fonts:
            f = os.path.basename(f_full)
            z.write(f_full, os.path.join("EPUB", "fonts", f))
            # ext = os.path.extsep(f)[1].lower()
            # if ext in [".ttf"]:
            #     media_type = "application/x-font-truetype"
            # else:
            #     raise Exception("Unsupported font type")
            # book.add_item(file_name=f, content=open(f_full, "rb").read(), media_type=media_type)
        z.close()



if __name__ == '__main__':
    main()


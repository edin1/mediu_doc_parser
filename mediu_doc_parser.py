from helpers import *
import create_epub_ebookmaker
import clean_up_html

WAIT = 3
DIRFORMAT = "%s-%s"
FNAMEFORMAT = "%s-P%04d%s"

STAGES = [
    "creating_initial_page_files",
    "copying_pages_content",
    # "close_original_document",
    "clean_up_pages",
    "convert_pages_to_html",
]
STAGES_NAMES = {
}
for i, stage in enumerate(STAGES):
    STAGES_NAMES[stage] = i

def main():
    parser = argparse.ArgumentParser(description='Parse MEDIU Microsoft Word (.doc/.docx) textbooks.')
    parser.add_argument('fpath', metavar='FILE',
                    help='The file to be parsed.')
    parser.add_argument("-f", '--first', dest='first_page', type=int,# default=1,
                        help='Number of the first page (e.g., if different from 1)')
    parser.add_argument("-l", '--log', dest='log', default="log.json",
                        help='JSON file used as log/database')

    args = parser.parse_args()

    if args.first_page is None:
        parser.error("You have to specify the number of the first page.")

    while True:
        word = open_word()
        try:
            parse_doc(args.fpath, args.first_page, args.log, word)
        except pywintypes.com_error as e:
            print("Caught pywintypes.com_error: ", e)
            time.sleep(3)
            word.Documents.Close(SaveChanges=wdDoNotSaveChanges)
            word.Quit()
        break


def parse_doc(fpath, first_page, log, word):
    fullpath = os.path.abspath(fpath)

    data = log_read(log)
    if "processing" not in data:
        data["processing"] = {}
    if fullpath not in data["processing"]:
        data["processing"][fullpath] = {}
    log_write(data, log)

    barename, ext = os.path.splitext(fullpath)
    basename, ext = os.path.splitext(os.path.basename(fullpath))

    # open existing document
    doc = word.Documents.Open(fullpath)
    word.ActiveDocument.ShowGrammaticalErrors = False
    word.ActiveDocument.ShowSpellingErrors = False

    print("="*40)
    print("Loading file %s..."%fullpath)
    print("="*40)

    # Making sure we're at the end of the document (so it's fully loaded)
    go_to_end(word)
    go_to_beginning(word)

    pages_num = word.ActiveDocument.BuiltInDocumentProperties("Number of Pages")
    # print(pages_num)
    pages_num = int(pages_num)
    print("Total pages:", pages_num)
    # pages_num = 3

    pages_range = range(first_page, pages_num + first_page, 1)

    # Creating the template for page files
    dir_doc = os.path.abspath(DIRFORMAT%(basename, ext[1:]))
    dir_create(dir_doc)
    tmp_path = os.path.join(dir_doc, FNAMEFORMAT%(basename, 0, ext))
    shutil.copy(fullpath, tmp_path)
    word.Documents.Open(tmp_path)
    word.ActiveDocument.Range().Delete()
    # Fix margins
    p = word.Selection.PageSetup
    p.TopMargin = InchesToPoints(1)
    p.BottomMargin = InchesToPoints(1)
    p.HeaderDistance = InchesToPoints(0.49)
    p.FooterDistance = InchesToPoints(0.49)
    word.ActiveDocument.Save()
    word.ActiveDocument.Close()

    def creating_initial_page_files():
        # print("Creating initial page files... ")
        # Creating new page-files with the same formatting as the original doc
        for page in pages_range:
            # print("Creating file for page %d"%page)
            # print(dir_doc)
            # print(FNAMEFORMAT)
            # print(basename)
            dest = os.path.join(dir_doc, FNAMEFORMAT%(basename, page, ext))
            # print(dest)
            shutil.copy(tmp_path, dest)

    def copying_pages_content():
        # print("Copying unchanged page content from original document... ")
        word.Application.Browser.Target = wdBrowsePage
        for page in pages_range:
            print("Copying content for page %d"%page)
            fullname_page = os.path.join(dir_doc, FNAMEFORMAT%(basename, page, ext))
            try:
                word.ActiveDocument.Bookmarks("\page").Range.Copy()
                word.Documents.Open(FileName=fullname_page)
                # word.ActiveDocument.Range().Delete()
                # word.Selection.Font.Name = "Arial"
                # word.Selection.Font.Size = 20
                # word.Selection.Font.Bold = True
                # word.Selection.Paragraphs.Alignment = wdAlignParagraphCenter
                # word.Selection.TypeText("[%d]"%page)
                # word.Selection.TypeParagraph()
                word.Selection.Paste()
                # remove_empty_paragraphs(word)
                #word.Selection.TypeBackspace()
                word.ActiveDocument.Save()
                word.ActiveDocument.Close()
                word.Application.Browser.Next()
            except pywintypes.com_error as e:
                if page == pages_range[-1]:
                    print("Caught com_error on last page while copying, ignoring...: ", e)
                    pass
                else:
                    raise

    def close_original_document():
        word.ActiveDocument.Close()

    def clean_up_pages():
        close_original_document()
        # print("Cleaning up all pages and adding page numbers...")
        # Now go through created documents and remove blank lines
        for page in pages_range:
            if page < data["processing"][fullpath]["page"]:
                continue
            print('Processing page %d' % page, end='... ')
            data["processing"][fullpath]["page"] = page
            log_write(data, log)
            word.Documents.Open(os.path.join(dir_doc, FNAMEFORMAT%(basename, page, ext)))
            remove_empty_at_end(word)

            convert_shapes_to_text(word)
            replace_all_fonts(word)
            # Add page numbers
            # for section in word.ActiveDocument.Sections:
            section = 1
            for option in (wdHeaderFooterFirstPage, wdHeaderFooterPrimary, wdHeaderFooterEvenPages):
                header = word.ActiveDocument.Sections(section).Headers(option)
                myrange = header.Range
                # print(repr(myrange))
                myrange.Paragraphs.Alignment = wdAlignParagraphCenter
                myrange.Font.Size = 20
                myrange.Text = "[%d]"%page
            # fix_margins(word)
            # Set whole page to be RTL
            word.Selection.WholeStory()
            word.Selection.RtlPara()
            word.ActiveDocument.Save()
            word.ActiveDocument.Close()
            print("Finished page: %d"%page)

    def convert_pages_to_html():
        # print("Converting to HTML...")
        dir_html = os.path.abspath(DIRFORMAT%(basename, "html"))
        dir_create(dir_html)
        for page in pages_range:
            word.Documents.Open(os.path.join(dir_doc, FNAMEFORMAT%(basename, page, ext)))
            save_filtered_html(word, os.path.join(dir_html, FNAMEFORMAT%(basename, page, ".html")))
            word.ActiveDocument.Close()
            print("Finished page: %d"%page)
        print("Creating epub file...")
        create_epub_ebookmaker.process_dir(clean_up_html.process_dir(dir_html))

    if "stage" not in data["processing"][fullpath]:
        data["processing"][fullpath]["stage"] = STAGES[0]
    if "page" not in data["processing"][fullpath]:
        data["processing"][fullpath]["page"] = pages_range[0]
    log_write(data, log)
    # creating_initial_page_files()
    # copying_pages_content()
    # clean_up_pages()
    for stage, stage_name in enumerate(STAGES):
        if stage < STAGES_NAMES[data["processing"][fullpath]["stage"]]:
            continue
        # else:
        print("-"*40)
        print("Stage %d/%d: "%(stage + 1, len(STAGES)), stage_name)
        print("-"*40)
        data["processing"][fullpath]["stage"] = stage_name
        log_write(data, log)
        locals()[stage_name]()
        data["processing"][fullpath]["page"] = pages_range[0]
        log_write(data, log)
    # convert_pages_to_html()
    del data["processing"][fullpath]
    log_write(data, log)
    # toplist = []
    # winlist = []
    # def enum_callback(hwnd, results):
    #     winlist.append((hwnd, win32gui.GetWindowText(hwnd)))

    # win32gui.EnumWindows(enum_callback, toplist)
    # wordwindows = [(hwnd, title) for hwnd, title in winlist if 'microsoft word' in title.lower()]
    # # print(wordwindows)
    # win32gui.SetForegroundWindow(wordwindows[0][0])

    # shell = win32com.client.Dispatch("WScript.Shell")
    # # shell.AppActivate("Microsoft Word")
    # shell.SendKeys("^a") # CTRL+A may "select all" depending on which window's focused
    # # shell.SendKeys("{DELETE}") # Delete selected text?  Depends on context. :P
    # # shell.SendKeys("{TAB}") #Press tab... to change focus or whatever
    # # print(word.ActiveDocument.ActiveWindow.Panes(1).Pages.Item(1))

if __name__ == "__main__":
    main()

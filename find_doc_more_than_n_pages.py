import sys
from helpers import *
word = open_word()

def main():
    parser = argparse.ArgumentParser(description='Find word files with more than n pages')
    parser.add_argument('dir', metavar='DIR',
                    help='The directory to be searched.')
    parser.add_argument("-n", '--number', dest='number', type=int, default=1,
                        help='The number of pages the file should have more than')

    args = parser.parse_args()
    d = os.path.abspath(args.dir)
    n = args.number
    process_dir(d, n)

def process_dir(d, n):
    files = os.listdir(d)
    files.sort()
    for f in files:
        f_full = os.path.join(d, f)
        if os.path.isdir(f):
            # print('"%s" is a dir, recursing into it...'%f_full)
            process_dir(f_full, n)
        elif os.path.splitext(f.lower())[1].lower() in (".doc", ".docx"):
            _n = get_pages_number(f_full)
            if _n > n:
                warning('"%s" has %d pages (more than specified %d)'%(f_full, _n, n))
            else:
                pass
                # ok('"%s" is OK'%f_full)

def get_pages_number(f):
    word.Documents.Open(f)
    go_to_end(word)
    pages_num = int(word.ActiveDocument.BuiltInDocumentProperties("Number of Pages"))
    word.ActiveDocument.Close()
    return pages_num


if __name__ == "__main__":
    main()

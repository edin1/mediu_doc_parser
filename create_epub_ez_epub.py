# coding=utf-8
import os
import argparse
import zipfile
import re
from collections import OrderedDict as OD

import bs4
import ez_epub


STYLE = """
BODY { text-align: justify;}

@font-face {
  font-family: "AL-Hotham";
  font-style: normal;
  font-weight: normal;
  src:url("fonts/al-hotham.ttf");
}

@font-face {
  font-family: "AL-Mateen";
  font-style: normal;
  font-weight: normal;
  src:url("fonts/AL-Mateen.ttf");
}

@font-face {
  font-family: "AGA Rasheeq Bold";
  font-style: normal;
  font-weight: normal;
  src:url("fonts/AGA Rasheeq Bold.ttf");
}

@font-face {
  font-family: "AGA Arabesque";
  font-style: normal;
  font-weight: normal;
  src:url("fonts/arabsq.ttf");
}

@font-face {
  font-family: "AGA-Arabesque";
  font-style: normal;
  font-weight: normal;
  src:url("fonts/arabsq.ttf");
}

p {
font-family: "AL-Hotham";
/*direction: rtl;
unicode-bidi:bidi-override;*/
}

"""

STYLE = re.sub("src\:.*\;", 'src:url("fonts/Scheherazade-Regular.ttf");', STYLE)

LANG = "ar"

FONT_EXTS = [".ttf",]

def get_html_body(f):
    return bs4.BeautifulSoup(open(f).read()).find('body')

def main():
    parser = argparse.ArgumentParser(description='Create epub from all html files in a directory.')
    parser.add_argument('path', default=".",
                    help='The file or directory path where to look for html files.')

    args = parser.parse_args()
    path = os.path.abspath(args.path)
    if os.path.isdir(path):
        process_dir(path)
    else:
        raise Exception("You have to supply a valid directory.")

def process_dir(d):
    files = os.listdir(d)
    files.sort()
    files_html = []
    files_fonts = []
    for f in files:
        f_full = os.path.abspath(os.path.join(d, f))
        if os.path.isdir(f):
            continue
        else:
            ext = os.path.splitext(f.lower())[1].lower()
            if ext in (".html", ".htm"):
                files_html.append(f_full)
            elif ext in FONT_EXTS:
                files_fonts.append(f_full)
    if not files_html:
        raise Exception("There are no html files in the directory you provided")
    # else:
    book = ez_epub.Book()
    book.lang = "ar"
    book.title = os.path.basename(files_html[0]).rsplit("-", 1)[0]
    # book.authors = authors

    pages = []
    for f_full in files_html:
        f = os.path.basename(f_full)
        # page number for now
        title_p = os.path.splitext(f)[0].split("-")[-1]
        # remove the P letter
        title_p = str(int(title_p[1:]))
        page = ez_epub.Section()
        page.text.extend(get_html_body(f_full))
        page.css = STYLE
        page.title = title_p
        pages.append(page)

    book.sections = pages
    book.make(r'%s' % book.title)

    # Add fonts
    if files_fonts:
        z = zipfile.ZipFile(title + ".epub", "a")
        for f_full in files_fonts:
            f = os.path.basename(f_full)
            z.write(f_full, os.path.join("EPUB", "fonts", f))
            # ext = os.path.extsep(f)[1].lower()
            # if ext in [".ttf"]:
            #     media_type = "application/x-font-truetype"
            # else:
            #     raise Exception("Unsupported font type")
            # book.add_item(file_name=f, content=open(f_full, "rb").read(), media_type=media_type)
        z.close()



if __name__ == '__main__':
    main()

